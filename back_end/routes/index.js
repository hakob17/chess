var express = require('express');
var router = express.Router();
var passport = require('passport');
var mongoose = require('mongoose');
var User = require('../models/user');

router.get('/', isLoggedIn, function (req, res) {
  res.render('auth/index');
});

router.get('/login', function (req, res) {
  res.render('auth/login', {message: req.flash('loginMessage')});
});

router.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.json({succes: false});
    }
    req.logIn(user, function (err) {
      if (err) {
        return res.json({succes: false});
      }
      var users = User.find({}).exec();
      var data = users.then(function (response) {

        return res.json({succes: true, users: response});
      });
    });
  })(req, res, next);
});

router.get('/signup', function (req, res) {

  // render the page and pass in any flash data if it exists
  res.render('auth/signup', {message: req.flash('signupMessage')});
});

router.post('/signup', passport.authenticate('local-signup', {
  successRedirect: '/profile', // redirect to the secure profile section
  failureRedirect: '/signup', // redirect back to the signup page if there is an error
  failureFlash: true // allow flash messages
}));

router.get('/profile', isLoggedIn, function (req, res) {
  var users = User.find({}).exec();
  var data = users.then(function (response) {
    res.render('user/profile', {
      user: req.user, // get the user out of session and pass to template
      users: response
    });
  });

  //console.log(User.find({}))
});

router.get('/users', function (req, res) {
  User.find({}, 'name', function (err, users) {
    if (err) {
      console.log(err);
    } else {
      return users;
      /*res.render('user-list', users);
       console.log('retrieved list of names', users.length, users[0].name);*/
    }
  })
})

router.get('/logout', function (req, res) {
  req.logout();
  res.redirect('/');
});

function isLoggedIn(req, res, next) {

  if (req.isAuthenticated())
    return next();

  res.redirect('/login');
}
module.exports = router;
