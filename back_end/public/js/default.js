/**
 * Created by hakob on 29.08.2016.
 */



var socket = io();

// called when a player makes a move on the board UI
var handleMove = function (source, target) {
  removeGreySquares();
  var move = game.move({from: source, to: target, promotion: 'q'});
  // illegal move
  if (move === null) return 'snapback';

  updateStatus();
  socket.emit('move', move);
}


socket.on('move', function (msg) {
  game.move(msg);
  console.log(msg)
  board.position(game.fen()); // fen is the board layout
});

statusEl = $('#status'),
  fenEl = $('#fen'),
  pgnEl = $('#pgn');

var initGame = function () {
  var cfg = {
    draggable: true,
    position: 'start',
    onDragStart: onDragStart,
    onDrop: handleMove,
    onMouseoutSquare: onMouseoutSquare,
    onMouseoverSquare: onMouseoverSquare,
    onSnapEnd: onSnapEnd
  };

  board = new ChessBoard('gameBoard', cfg);
  game = new Chess();

}
var onDragStart = function (source, piece, position, orientation) {
  if (game.game_over() === true ||
    (game.turn() === 'w' && piece.search(/^b/) !== -1) ||
    (game.turn() === 'b' && piece.search(/^w/) !== -1)) {
    return false;
  }
};
// update the board position after the piece snap
// for castling, en passant, pawn promotion
var onSnapEnd = function () {
  board.position(game.fen());
};

var updateStatus = function () {
  var status = '';

  var moveColor = 'White';
  if (game.turn() === 'b') {
    moveColor = 'Black';
  }

  // checkmate?
  if (game.in_checkmate() === true) {
    status = 'Game over, ' + moveColor + ' is in checkmate.';
  }

  // draw?
  else if (game.in_draw() === true) {
    status = 'Game over, drawn position';
  }

  // game still on
  else {
    status = moveColor + ' to move';

    // check?
    if (game.in_check() === true) {
      status += ', ' + moveColor + ' is in check';
    }
  }

  statusEl.html(status);
  fenEl.html(game.fen());
  pgnEl.html(game.pgn());
};

var removeGreySquares = function () {
  $('#gameBoard .square-55d63').css('background', '');
};

var greySquare = function (square) {
  var squareEl = $('#gameBoard .square-' + square);

  var background = '#a9a9a9';
  if (squareEl.hasClass('black-3c85d') === true) {
    background = '#696969';
  }
  console.log(squareEl)
  squareEl.css('background', background);
};

var onMouseoverSquare = function (square, piece) {
  // get list of possible moves for this square
  var moves = game.moves({
    square: square,
    verbose: true
  });

  // exit if there are no moves available for this square
  if (moves.length === 0) return;

  // highlight the square they moused over
  greySquare(square);

  // highlight the possible squares for this piece
  for (var i = 0; i < moves.length; i++) {
    greySquare(moves[i].to);
  }
};

var onMouseoutSquare = function (square, piece) {
  removeGreySquares();
};

var onSnapEnd = function () {
  board.position(game.fen());
};




initGame();
