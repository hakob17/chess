'use strict';

/**
 * @ngdoc service
 * @name chessApp.Socket
 * @description
 * # Socket
 * Factory in the chessApp.
 */
angular.module('chessApp')
  .factory('Socket', function (socketFactory,$rootScope) {
    var socket = io.connect('http://localhost:3000');
    return {
      on: function (eventName, callback) {
        socket.on(eventName, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        })
      }
    };
  });
