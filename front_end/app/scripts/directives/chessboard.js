'use strict';

/**
 * @ngdoc directive
 * @name chessApp.directive:chessBoard
 * @description
 * # chessBoard
 */
angular.module('chessApp')
  .directive('chessBoard', function (Socket) {
    return {
      template: '<div id="gameBoard" style="width: 600px; height: 600px;background: #235678; margin: 0 auto;"></div><p>PGN: <span id="pgn"></span></p>',
      restrict: 'E',
      scope: {
        myturn: '=',
        iam : '=',
        update: '&update'
      },
      link: function postLink(scope, element, attrs) {
        var socket = Socket;

        var cfg = {
          draggable: true,
          position: 'start',
          onDragStart: onDragStart,
          onDrop: handleMove,
          onMouseoutSquare: onMouseoutSquare,
          onMouseoverSquare: onMouseoverSquare,
          onSnapEnd: onSnapEnd
        };
        var board = new ChessBoard('gameBoard', cfg);
        var game = new Chess();
        socket.on('move', function (msg) {
          console.log(msg)
          game.move(msg);
          board.position(game.fen()); // fen is the board layout
        });


        function handleMove(source, target) {
          removeGreySquares();
          var move = {};
          move.move = game.move({from: source, to: target, promotion: 'q'});
          // illegal move
          if (move.move === null) return 'snapback';

          updateStatus();
          move.turn = game.turn();
          console.log(move)
          socket.emit('move', move);
        }

        var statusEl = angular.element('#status');
        var fenEl = angular.element('#fen');
        var pgnEl = angular.element('#pgn');

        function onDragStart(source, piece, position, orientation) {

          if (game.game_over() === true || game.turn() !== scope.iam ||
            (game.turn() === 'w' && piece.search(/^b/) !== -1) ||
            (game.turn() === 'b' && piece.search(/^w/) !== -1) || !scope.myturn) {
            return false;
          }
        };

        function updateStatus() {
          var status = '';

          var moveColor = 'White';
          if (game.turn() === 'b') {
            moveColor = 'Black';
          }

          // checkmate?
          if (game.in_checkmate() === true) {
            status = 'Game over, ' + moveColor + ' is in checkmate.';
          }

          // draw?
          else if (game.in_draw() === true) {
            status = 'Game over, drawn position';
          }

          // game still on
          else {
            status = moveColor + ' to move';

            // check?
            if (game.in_check() === true) {
              status += ', ' + moveColor + ' is in check';
            }
          }

          scope.update();
          scope.myturn = !scope.myturn;

          statusEl.html(status);
          fenEl.html(game.fen());
          pgnEl.html(game.pgn());
        };

        function removeGreySquares() {
          angular.element('#gameBoard .square-55d63').css('background', '');
        };

        function greySquare(square) {
          var squareEl = angular.element('#gameBoard .square-' + square);

          var background = '#a9a9a9';
          if (squareEl.hasClass('black-3c85d') === true) {
            background = '#696969';
          }
          squareEl.css('background', background);

        };

        function onMouseoverSquare(square, piece) {
          if (scope.myturn || game.turn() === scope.iam) {
            var moves = game.moves({
              square: square,
              verbose: true
            });

            if (moves.length === 0) return;

            greySquare(square);

            for (var i = 0; i < moves.length; i++) {
              greySquare(moves[i].to);
            }
          }
        };

        function onMouseoutSquare(square, piece) {
          removeGreySquares();
        };

        function onSnapEnd() {
          board.position(game.fen());
        };

      }
    };
  });
