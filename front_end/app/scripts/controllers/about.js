'use strict';

/**
 * @ngdoc function
 * @name chessApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the chessApp
 */
angular.module('chessApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
