'use strict';

/**
 * @ngdoc function
 * @name chessApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the chessApp
 */
angular.module('chessApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
