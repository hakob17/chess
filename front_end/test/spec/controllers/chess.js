'use strict';

describe('Controller: ChessctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('frontEndApp'));

  var ChessctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ChessctrlCtrl = $controller('ChessctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ChessctrlCtrl.awesomeThings.length).toBe(3);
  });
});
